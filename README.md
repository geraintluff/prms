# prms.js

## Small promises polyfill (MDN), plus .callback() enhancement

This is a fairly small implementation of Promise (1413 bytes minified, about half that gzipped).

In the browser, if `Promise` is not in the global scope, it provides an implementation that matches the MDN [Promise API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise), and is [A+ compatible](https://github.com/promises-aplus/promises-spec).

When included as a module, either the native implementation or the polyfill are returned (and the global scope is not modified).

The following methods are added to either the native implementation or the polyfill:

## Promise.callback(function (callback) {...})

An alternative way to construct promises, that gives you a callback function instead of pass/fail.  This lets you return a Promise while using async APIs:

```javascript
var Promise = require('prms');

return Promise.callback(function (callback) {
    fs.readFile(filename, {encoding: 'utf-8'}, callback);
});
```

## Promise.prototype.callback(callback)

This is the equivalent to `.then()`, but for Node.js-style callbacks.  This lets you implement an async API that uses Promises internally.  It also makes it easy to construct hybrid promise/callback APIs:

```javascript
var Promise = require('prms');

function myAsyncApi(arg0, callback) {
    var promise = Promise.resolve(somePrmsApi(...)); // wraps any thenable in Promise
    return promise.then(function (result) {
        return somethingElse(result);
    }).callback(callback);
}
```

The `callback` argument has no effect on the result of the promise.  In fact `callback` can be omitted - therefore in the above example, the API can be used in either an async or Promise-y manner.

Because `callback` is called on both resolution and rejection, it is also a convenient way to perform cleanup:

```javascript
var Promise = require('prms');

var connection = openSomething();
Promise.resolve(somePromiseApi(...)).then(function (result) {
    ...
}).callback(function () {
    // cleanup code
    connection.close();
});
```

A series of callbacks will be called in the order they are registered, but their return values are ignored - this means that if your cleanup is asynchronous, it will overlap with subsequent callbacks.
