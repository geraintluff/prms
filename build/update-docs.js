var fs = require('fs');

var minified = fs.readFileSync(process.argv[2]);
var minifiedBytes = minified.length;

console.log('\nMinified length: ' + minifiedBytes + ' bytes');

process.argv.slice(3).forEach(function (filename) {
    var text = fs.readFileSync(filename, {encoding: 'utf-8'});
    text = text.replace(/[0-9]+\s*bytes minified/, minifiedBytes + ' bytes minified');
    fs.writeFileSync(filename, text);
    console.log('\tupdated ' + filename);
});

console.log('');
