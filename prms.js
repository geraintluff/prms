Promise = (function (Promise) {
    function isFunction(value) {
        return typeof value === 'function';
    }

    function catchFunction(onFail) {
        this.then(0, onFail);
    }
    var asapList = [];
    function asap(fn) {
        if (!asapList[0]) {
            setTimeout(function () {
                while (asapList[0]) asapList.shift()();
            }, 0);
        }
        asapList.push(fn);
    }

    if (!Promise) {
        Promise = function Promise(initFunction) {
            var thisPromise = this;
            var arg;

            var passHandlers = [];
            var failHandlers = [];
            var selectedHandlers;
            function checkHandlers() {
                while (selectedHandlers && selectedHandlers[0]) {
                    selectedHandlers.shift()(arg);
                }
            }

            thisPromise.then = function (onPass, onFail) {
                asap(checkHandlers);

                return new Promise(function (pass, fail) {
                    function tryCatch(handler) {
                        return function (arg) {
                            try {
                                pass(handler(arg));
                            } catch (e) {
                                fail(e);
                            }
                        };
                    }
                    passHandlers.push(isFunction(onPass) ? tryCatch(onPass) : pass);
                    failHandlers.push(isFunction(onFail) ? tryCatch(onFail) : fail);
                });
            };
            thisPromise.catch = catchFunction;

            function resolve(value) {
                asap(checkHandlers);
                arg = value;
                selectedHandlers = passHandlers;
            }
            function reject(reason) {
                resolve(reason);
                selectedHandlers = failHandlers;
            }

            var passCallCount = 0;
            initFunction(function pass(value) {
                if (passCallCount++) return;
                if (value === thisPromise) {
                    reject(new TypeError('loop'));
                } else {
                    var valueThen;
                    try {
                        valueThen = value && (typeof value === 'object' || isFunction(value)) && value.then;
                    } catch (e) {
                        return reject(e);
                    }
                    if (isFunction(valueThen)) {
                        ((value instanceof Promise) ? value : new Promise(function (pass, fail) {
                            try {
                                valueThen.call(value, pass, fail);
                            } catch (e) {
                                fail(e);
                            }
                        })).then(resolve, reject);
                    } else {
                        resolve(value);
                    }
                }
            }, function (reason) {
                if (passCallCount++) return;
                reject(reason);
            });
        }
        Promise.resolve = function (value) {
            return new Promise(function (pass) {
                pass(value);
            });
        };
        Promise.reject = function (reason) {
            return new Promise(function (pass, fail) {
                fail(reason);
            });
        };
        Promise.all = function (arr) {
            return new Promise(function (pass, fail) {
                var pending = arr.length + 1;
                var checks = [];
                var result = [];
                for (var i = 0; i < arr.length; i++) {
                    (function (i) {
                        Promise.resolve(arr[i]).then(function (value) {
                            result[i] = value;
                            if (!checks[i]) {
                                if (!--pending) pass(result);
                            }
                            checks[i] = 1;
                        }, fail);
                    })(i);
                }
                if (!--pending) pass(result);
            });
        };
        Promise.race = function (arr) {
            return new Promise(function (pass, fail) {
                for (var i = 0; i < arr.length; i++) {
                    arr[i].then(pass, fail);
                }
            });
        };
    }

    // callback

    Promise.prototype.callback = function (callback) {
        if (callback) this.then(function (result) {
            return callback(null, result), result;
        }, callback);
        return this;
    };
    Promise.callback = function (callbackInit) {
        return new Promise(function (pass, fail) {
            callbackInit(function (error, result) {
                if (error) {
                    fail(error);
                } else {
                    pass(result);
                }
            });
        });
    };

    return Promise;
})(typeof Promise === 'function' && Promise);

if (typeof module === 'object') module.exports = Promise;
