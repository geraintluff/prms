// Guarantee that we use our implementation
global.Promise = null;

var assert = require('chai').assert;

[{
    name: 'basic',
    module: require('../')
}].forEach(function (obj) {
    var name = obj.name;
    var api = obj.module;

    describe('MDN API (' + name + ')', function () {
        it('Promise.all() resolving', function (done) {
            assert.isFunction(api.all);

            var check = {a: false, b: false, c: false};
            var promises = {
                a: new api(function (pass, fail) {
                    pass('A')
                }),
                b: new api(function (pass, fail) {
                    pass('B')
                }),
                c: new api(function (pass, fail) {
                    pass('C')
                })
            };
            promises.a.then(function () {
                check.a = true;
            });
            promises.b.then(function () {
                check.b = true;
            });
            promises.c.then(function () {
                check.c = true;
            });

            assert.isFalse(check.a);
            assert.isFalse(check.b);
            assert.isFalse(check.c);

            api.all([promises.a, promises.b, promises.c]).then(function (result) {
                assert.deepEqual(result, ['A', 'B', 'C']);
                assert.isTrue(check.a);
                assert.isTrue(check.b);
                assert.isTrue(check.c);
                done();
            }, function () {
                throw new Error('api should not reject');
            });
        });

        it('Promise.all() rejecting', function (done) {
            assert.isFunction(api.all);

            var check = {a: false, b: false, c: false};
            var promises = {
                a: new api(function (pass, fail) {
                    pass('A')
                }),
                b: new api(function (pass, fail) {
                    fail(new Error('B'));
                }),
                c: new api(function (pass, fail) {
                    pass('C')
                })
            };
            promises.a.then(function () {
                check.a = true;
            });
            promises.b.then(function () {
                check.b = true;
            });
            promises.c.then(function () {
                check.c = true;
            });

            assert.isFalse(check.a);
            assert.isFalse(check.b);
            assert.isFalse(check.c);

            api.all([promises.a, promises.b, promises.c]).catch(function (reason) {
                assert.equal(reason.message, 'B');
                done();
            });
        });

        it('Promise.race() resolve', function (done) {
            api.race([
                new api(function (pass, fail) {
                    setTimeout(pass.bind(null, 'A'), 200);
                }),
                new api(function (pass, fail) {
                    setTimeout(fail.bind(null, new Error('B')), 100);
                }),
                new api(function (pass, fail) {
                    setTimeout(pass.bind(null, 'C'), 0);
                })
            ]).then(function (result) {
                assert.deepEqual(result, 'C');
                done();
            });
        });

        it('Promise.race() reject', function (done) {
            api.race([
                new api(function (pass, fail) {
                    setTimeout(pass.bind(null, 'A'), 200);
                }),
                new api(function (pass, fail) {
                    setTimeout(fail.bind(null, new Error('B')), 0);
                }),
                new api(function (pass, fail) {
                    setTimeout(pass.bind(null, 'C'), 100);
                })
            ]).catch(function (reason) {
                assert.deepEqual(reason.message, 'B');
                done();
            });
        });
    });
});
