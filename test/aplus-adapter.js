// Guarantee that we use our implementation
global.Promise = null;

var api = require('../');

module.exports = {
    resolved: function (value) {
        return api.resolve(value);
    },
    rejected: function (error) {
        return api.reject(error);
    },
    deferred: function (promise, resolve, reject) {
        var obj = {};
        obj.promise = new api(function (pass, fail) {
            obj.resolve = pass;
            obj.reject = fail;
        });
        return obj;
    }
};
