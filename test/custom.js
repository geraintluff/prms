// Guarantee that we use our implementation
global.Promise = null;

var assert = require('chai').assert;

var api = require('../');

describe('Custom API', function () {
    it('Promise.prototype.callback() resolving', function (done) {
        var promise = new api(function (pass, fail) {
            setTimeout(function () {
                pass('foo');
            }, 0);
        });
        promise.callback(function (error, result) {
            assert.deepEqual(error, null);
            assert.deepEqual(result, 'foo');
            done();
        });
    });

    it('Promise.prototype.callback() rejecting', function (done) {
        var promise = new api(function (pass, fail) {
            setTimeout(function () {
                fail(new Error('foo'));
            }, 0);
        });
        promise.callback(function (error, result) {
            assert.deepEqual(error.message, 'foo');
            done();
        });
    });

    it('Promise.callback() resolving', function (done) {
        api.callback(function (callback) {
            setTimeout(function () {
                callback(null, 'bar');
            }, 0);
        }).then(function (value) {
            assert.deepEqual(value, 'bar');
            done();
        });
    });

    it('Promise.callback() rejecting', function (done) {
        api.callback(function (callback) {
            setTimeout(function () {
                callback(new Error('bar'));
            }, 0);
        }).catch(function (error) {
            assert.deepEqual(error.message, 'bar');
            done();
        });
    });
});
